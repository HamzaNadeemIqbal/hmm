(function(root) {
    'use strict';
  
    function HiddenMarkovModel() {
      if (!(this instanceof HiddenMarkovModel)) {
        return new HiddenMarkovModel();
      }
  
      this.initialStateVector = [];
      this.currentStateVector = [];
      this.transitionStateMatrix = [];
      this.emissionStateMatrix = [];
      this.iterationsCount = 0;
    }
  
    HiddenMarkovModel.prototype.setInitialStateVector = function(vector) {
      this.initialStateVector = vector;
      this.currentStateVector = vector;
    };
  
    HiddenMarkovModel.prototype.getInitialStateVector = function() {
      return this.initialStateVector;
    };
  
    HiddenMarkovModel.prototype.setTransitionMatrix = function(matrix) {
      this.transitionStateMatrix = matrix;
    };
  
    HiddenMarkovModel.prototype.getTransitionMatrix = function() {
      return this.transitionStateMatrix;
    };
  
    HiddenMarkovModel.prototype.setEmissionMatrix = function(matrix) {
      this.emissionStateMatrix = matrix;
    };
  
    HiddenMarkovModel.prototype.getEmissionMatrix = function() {
      return this.emissionStateMatrix;
    };
  
    HiddenMarkovModel.prototype.forward = function(observedSequence, alpha) {
      alpha =  alpha || [];
      var PI = this.initialStateVector;
      var A = this.transitionStateMatrix;
      var B = this.emissionStateMatrix;
      var O = observedSequence || [];
      var T = O.length;
      var N = A.length;
      var probability = 0;
  
      if (!Array.isArray(O)) {
        throw new TypeError('Emssion sequence must be an array');
      }
  
      if (!Array.isArray(alpha)) {
        throw new TypeError('Alpha must be an array');
      }
  
      if (!O.length) {
        return probability;
      }
  
      // Initialization
      for (var i = 0; i < N; i++) {
        alpha[i] = [];
        /*
         * alpha[0][0] = P(U|S) * P(Rej|U)
         * alpha[0][1] = P(C|S) * P(Rej|C)
         */
         alpha[0][i] = PI[i] * B[i][O[0]];
      }
  
      // Recursion
      for (var i = 1; i < T; i++) {
        alpha[i] = [];
        for (var j = 0; j < N; j++) {
          /*
           * ALPHA[1][0] = MAX(alpha1(U) * P(U|U) * P(App|U)) OR (alpha1(C) + P(U|C) * P(App|U))
           * ALPHA[1][1] = MAX(alpha1(U) * P(C|C) * P(App|C)) OR (alpha1(C) + P(C|C) * P(App|C))
           * 
           * ALPHA[2][0] = MAX(alpha2(U) * P(U|U) * P(Acp|U)) OR (alpha2(C) + P(U|C) * P(Acp|U))
           * ALPHA[2][1] = MAX(alpha2(U) * P(C|U) * P(Acp|C)) OR (alpha2(C) + P(C|C) * P(Acp|C))
           * 
           */
          var result = 0;
        var currentResult = 0;
        for (var l = 0; l < N; l++) {
                      result = alpha[i-1][l] * A[l][j] * B[j][O[i]];
                      if(result > currentResult) {
                          currentResult = result;
                      }
                    }
                    alpha[i][j] = currentResult;
        }
      }
  
      // Termination
      /*
       * Probability = alpha3(U) + alpha3(C) 
      */
      for (var i = 0; i < N; i++) {
        probability += alpha[T-1][i];
      }
  
      return probability;
    };
  
    // Error Checking

    // if (typeof exports !== 'undefined') {
    //   if (typeof module !== 'undefined' && module.exports) {
    //     exports = module.exports = HiddenMarkovModel;
    //   }
    //   exports.HiddenMarkovModel = HiddenMarkovModel;
    // } else if (typeof define === 'function' && define.amd) {
    //   define([], function() {
    //     return HiddenMarkovModel;
    //   });
    // } else {
    //   root.HiddenMarkovModel = HiddenMarkovModel;
    // }
  
  })(this);
  