var HiddenMarkovModel = require('hidden-markov-model');
 
var HMM = HiddenMarkovModel();
 
HMM.setInitialStateVector([0.2, 0.8]); // 1. Unique, 2. Common 
 
HMM.setTransitionMatrix([
    // Unique, Common
    [0.7, 0.3], // Unique 
    [0.1, 0.9]  // Common
]);
 
HMM.setEmissionMatrix([
    // 1-Rejected 2-Appreciate, 3-Accepted 
    [0.6, 0.2, 0.2], // Uniqu e 
    [0.2, 0.3, 0.5]  // Common
]);
 
  /* What is the probability that the Hidden Markov Model is able to
   * generate the observed sequence of Rejected Appreciated Accepted?
   */
  var alpha = [];
  var result = HMM.forward([0, 1, 2], alpha); //Rejected Appreciated Accepted
  console.log( "Likelihood of the observed sequence Rej, App, Acc :" , result); // 0.03118
  console.log();


  console.log(alpha);
//   [ [ 0.12, 0.16],
//   [ 0.02, 0.054 ],
//   [ 0.00388, 0.0273 ] ]