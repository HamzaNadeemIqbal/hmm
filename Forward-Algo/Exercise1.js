var HiddenMarkovModel = require('hidden-markov-model');
 
var HMM = HiddenMarkovModel();
 
HMM.setInitialStateVector([0.8, 0.2, 0]); // 1. Unique, 2. Common 
 
HMM.setTransitionMatrix([
    // Unique, Common
    [0.6, 0.3, 0.1], // Unique 
    [0.4, 0.5, 0.1], // Unique 
    [0, 0, 0]  // Common
]);
 
HMM.setEmissionMatrix([
    // 1-Rejected 2-Appreciate, 3-Accepted 
    [0.2, 0.4, 0.4], // Uniqu e 
    [0.5, 0.4, 0.1],
    [0, 0, 0]  // Common
]);
 
  /* What is the probability that the Hidden Markov Model is able to
   * generate the observed sequence of Rejected Appreciated Accepted?
   */
  var alpha = [];
  var result = HMM.forward([2, 2, 0, 0, 1, 1, 2, 0, 2], alpha); //Rejected Appreciated Accepted
  console.log(result); // 0.03118
 
  console.log(alpha);
//   [ [ 0.12, 0.16],
//   [ 0.02, 0.054 ],
//   [ 0.00388, 0.0273 ] ]