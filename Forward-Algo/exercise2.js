var HiddenMarkovModel = require('hidden-markov-model');
 
var HMM = HiddenMarkovModel();
 
HMM.setInitialStateVector([0.8, 0.2, 0]);
 
HMM.setTransitionMatrix([
    [0.6, 0.3, 0.1],
    [0.4, 0.5, 0.1],  
    [0, 0, 0]
]);
 
HMM.setEmissionMatrix([
    [0.2, 0.4, 0.4], 
    [0.5, 0.4, 0.1],
    [0, 0, 0]  
]);
 
  var alpha = [];
  var resulting_matrix = HMM.forward([2, 2, 0, 0, 1, 1, 2, 0, 2], alpha); // 331122313
  console.log(resulting_matrix);
 
  console.log(alpha);