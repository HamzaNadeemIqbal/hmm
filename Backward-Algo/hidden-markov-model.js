(function(root) {
  'use strict';

  function HiddenMarkovModel() {
    if (!(this instanceof HiddenMarkovModel)) {
      return new HiddenMarkovModel();
    }

    this.initialStateVector = [];
    this.currentStateVector = [];
    this.transitionStateMatrix = [];
    this.emissionStateMatrix = [];
    this.iterationsCount = 0;
  }

  HiddenMarkovModel.prototype.setInitialStateVector = function(vector) {
    this.initialStateVector = vector;
    this.currentStateVector = vector;
  };

  HiddenMarkovModel.prototype.getInitialStateVector = function() {
    return this.initialStateVector;
  };

  HiddenMarkovModel.prototype.setTransitionMatrix = function(matrix) {
    this.transitionStateMatrix = matrix;
  };

  HiddenMarkovModel.prototype.getTransitionMatrix = function() {
    return this.transitionStateMatrix;
  };

  HiddenMarkovModel.prototype.setEmissionMatrix = function(matrix) {
    this.emissionStateMatrix = matrix;
  };

  HiddenMarkovModel.prototype.getEmissionMatrix = function() {
    return this.emissionStateMatrix;
  };

  HiddenMarkovModel.prototype.forward = function(observedSequence, beta) {
    beta =  beta || [];
    var PI = this.initialStateVector;
    var A = this.transitionStateMatrix;
    var B = this.emissionStateMatrix;
    var O = observedSequence || [];
    var T = O.length;
    var N = A.length;
    var probability = 0;

    if (!Array.isArray(O)) {
      throw new TypeError('Emssion sequence must be an array');
    }

    if (!Array.isArray(beta)) {
      throw new TypeError('beta must be an array');
    }

    if (!O.length) {
      return probability;
    }

    for (var row = 0; row < T; row++) {
      beta[row] = [];
    }  

    // Initialization
    for (var i = 0; i < N; i++) {
       beta[T-1][i] = 1;
    }

    // Recursion
    for (var time = T-2; time >=0; time--) {
      for (var column = 0; column < N; column++) {
        var result = 0;
        for (var l = 0; l < N; l++) {
          result += beta[time+1][l] * A[column][l] * B[l][O[time+1]];
        }
        beta[time][column] = result;
      }
    }

    // Termination
    for (var i = 0; i < N; i++) {
    
      probability += beta[0][i] * PI[i] *  B[i][O[0]];
    }
    return probability;
  };
  
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = HiddenMarkovModel;
    }
    exports.HiddenMarkovModel = HiddenMarkovModel;
  } else if (typeof define === 'function' && define.amd) {
    define([], function() {
      return HiddenMarkovModel;
    });
  } else {
    root.HiddenMarkovModel = HiddenMarkovModel;
  }

})(this);
