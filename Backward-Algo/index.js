var HiddenMarkovModel = require('hidden-markov-model');
 
var HMM = HiddenMarkovModel();
 
HMM.setInitialStateVector([0.99, 0.01]); // 1. Unique, 2. Common 
 
HMM.setTransitionMatrix([
    // A, B
    [0.99, 0.01], // A 
    [0.01, 0.99]  // B
]);
 
HMM.setEmissionMatrix([
    // O1, O2, O3 
    [0.8, 0.2, 0.8], // A 
    [0.1, 0.9, 0.1]  // B
]);
 
  /* What is the probability that the Hidden Markov Model is able to
   * generate the observed sequence of Rejected Appreciated Accepted?
   */
  var beta = [];
  var result = HMM.forward([0, 1, 2], beta); //Rejected Appreciated Accepted
  console.log( "Likelihood of the observed sequence 0 1 0 :" , result); // 0.03118
  console.log();


  console.log(beta);
//   [ [ 0.12, 0.16],
//   [ 0.02, 0.054 ],
//   [ 0.00388, 0.0273 ] ]